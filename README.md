Local Main
==========

Docker image for starting/stopping the local development environment.

Installation
------------

1. Install Docker: https://www.docker.com/community-edition#download
2. Create a directory where you want to store your projects, for example C:\Users\veijo\work

Starting the development environment
------------------------------------

* Windows: Run PowerShell as an administrator and in there run 
  (replacing /c/Users/veijo/work with your work directory,
  changing ``C:`` to ``/c`` and ``\ `` to ``/``)

    ```bash
    docker run \
      -v /var/run/docker.sock:/var/run/docker.sock \
      -e WORK_DIR=/c/Users/veijo/work \
      registry.gitlab.com/wysiwygoy/dev/local
    ```

* Mac: From command line shell, run

    ```bash
    docker run \
      -v /var/run/docker.sock:/var/run/docker.sock \
      -e WORK_DIR=[absolute path to your work folder] \
      registry.gitlab.com/wysiwygoy/dev/local
    ```

* Open http://local.wysiwyg.fi in a browser

Stopping the development environment
------------------------------------

* From command line shell, run

    ```bash
    docker run \
      -v /var/run/docker.sock:/var/run/docker.sock \
      registry.gitlab.com/wysiwygoy/dev/local \
      down
    ```

Upgrading to the latest version
-------------------------------

```bash
docker run \
      -v /var/run/docker.sock:/var/run/docker.sock \
      registry.gitlab.com/wysiwygoy/dev/local \
      pull
```

Then (re)start the development environment as described above.

How it works
------------

This image runs docker-compose that starts up jwilder/nginx-proxy
and local-ui. nginx-proxy is mapped to host port 80 and it
delivers the requests to appropriate containers in *.local.wysiwyg.fi.

Host names local.wysiwyg.fi and *.local.wysiwyg.fi resolve to localhost (127.0.0.1).

Local folder structures
-----------------------

All projects in the local development environment must be under one common folder
and have this kind of folder structures for the tooling to work: 

```
(work folder)                Contains the project folders, can be named freely.
  ┣━ some-project            Project folder, name matches the project slug.
  ┃   ┗━ *                   There can be different kinds of project structures:
  ┃
  ┣━ legacy-project          Legacy projects don't use composer or any kind of build process.
  ┃   ┣━ site                This folder contains the files that will be deployed (copied) to the server.
  ┃   ┃   ┃                  These can be edited directly and they are all stored in version control.
  ┃   ┃   ┣━ public          Files in this folder will be copied to public_html (or whatever www-root there is).
  ┃   ┃   ┗━ *               Other files will be copied to the parent folder of public_html.
  ┃   ┃                      This allows separating publicly available files from those needed only internally.
  ┃   ┃                      Although in legacy projects everything typically is public.
  ┃   ┣━ config              Various configuration files.
  ┃   ┃   ┗━ nginx           Configuration files for nginx. This is mapped to /etc/nginx/conf.d on nginx container.
  ┃   ┃        ┗━ site.conf  The configuration file for nginx.
  ┃   ┣━ deployment          Contains files that define how project is deployed, name is fixed.
  ┃   ┣━ docker-compose.yml  Docker-compose configuration for running the project in Docker.
  ┃   ┣━ .env                Environment variables for running the project in Docker. Not stored in version control.
  ┃   ┣━ .gitignore          Files ignored by Git.
  ┃   ┗━ *                   Other files as needed.
  ┃
  ┣━ another-one             Other projects.
  ┗━ …
```
