#
# Docker image for starting/stopping the local development environment.
#
# This is just docker-compose with preconfigured docker-compose.yml.
#

FROM docker/compose:1.18.0
MAINTAINER Jarno Antikainen <jarno.antikainen@wysiwyg.fi>

LABEL Description="Docker image for starting/stopping the local development environment." Vendor="Wysiwyg Oy"

COPY docker-compose.yml .
ENTRYPOINT ["docker-compose", "--project-name", "wysiwyg_dev"]
CMD ["up", "-d"]

